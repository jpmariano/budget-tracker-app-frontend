import { Fragment, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// React-Bootstrap
import { Row, Col, Form, Button } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// SweetAlert2 
import Swal from 'sweetalert2';

// Create new category page
export default function CreateCategory() {
	const headData = {
		title: 'Create A New Category',
		description: 'Segregate your income from your expenses.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData}/>
			<Row className="justify-content-center">
				<Col xs md="6">
					<NewCategory />
				</Col>
			</Row>
		</Fragment>
	);
}

// Form
const NewCategory = () => {
	// router
	const router = useRouter();
	
	// State
	const [type, setType] = useState('');
	const [name, setName] = useState('');
	const [isActive, setIsActive] = useState(false);

	// Category creation process
	function createCategory(e) {
		e.preventDefault();
		
		// let token = localStorage.getItem('token');
		
		// To create or add new category
		fetch(`${AppHelper.API_URL}/users/add-category`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				type: type,
				name: name
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				setType('');
				setName('');
				
				Swal.fire('Success!', 'Congrats, you\'re one step closer to managing your finances.', 'success');
				
				// redirect
				router.push('/categories');
			} else {
				Swal.fire('Oops...', 'Something went wrong. Please try again.', 'error');
			}
		});
	}
	
	// Effect
	useEffect(() => {
		if (type !== '' && name !== '') {
			setIsActive(true);
		}
	}, [type, name]);
	
	return (
		<Fragment>
			<h2>Create A New Category</h2>
			<Form onSubmit={ createCategory }>
				<Form.Group controlId="categoryType">
					<Form.Label>Category Type:</Form.Label>
					<Form.Control
						as="select"
						value={ type }
						onChange={(e) => setType(e.target.value)}
						required
					>
						<option disabled value="">Select a category</option>
						<option value="Expense">Expense</option>
						<option value="Income">Income</option>
					</Form.Control>
				</Form.Group>
			
				<Form.Group controlId="categoryName">
					<Form.Label>Category Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter category name"
						value={ name }
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				
				{ isActive ?
					<Button className="mb-3 btn btn-block" variant="primary" type="submit">
						Submit
					</Button>
					:
					<Button disabled className="mb-3 btn btn-block" variant="secondary">
						Submit
					</Button>
				}
				
				<Button href="/categories" className="mb-3 btn btn-block" variant="dark">
					Back
				</Button>
			</Form>
		</Fragment>
	);
};