import { Fragment, useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// Monthly expenses
export default function MonthlyIncome() {
	// State
	const [records, setRecords] = useState([]);
	const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);
	
	// Effect
	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			if (typeof user !== 'undefined') {
				setRecords(user.records);
			}
		});
	}, []);
	
	useEffect(() => {
		setMonthlyIncome(months.map(month => {
			let total = 0;
			
			records.forEach(element => {
				if ((moment(element.createdOn).format('MMMM') === month) && element.type === "Income") {
					total += element.amount;
				}
			});
			
			return total;
		}));
	}, [records]);
	
	const data = {
		labels: months,
		datasets: [{
			label: 'Monthly Expenses for the year 2021',
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyIncome
		}]
	};
	
	const headData = {
		title: 'Monthly Income',
		description: 'Take note of how much you are earning.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<h2>Monthly Income</h2>
			<Bar data={ data } />
		</Fragment>	
	);
}