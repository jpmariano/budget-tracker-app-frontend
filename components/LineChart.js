import { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';

export default function LineChart({ budgetData }) {
	// State
	const [date, setDate] = useState([]);
	const [amount, setAmount] = useState([]);
	
	// Effect
	useEffect(() => {
		if (budgetData.length > 0) {
			setDate(budgetData.map(element => element.date));
			setAmount(budgetData.map(element => element.amount));
		}
	}, [budgetData]);

	const data = {
		labels: date,
		datasets: [{
			label: 'Daily Trend',
			data: amount,
			backgroundColor: 'slateblue',
			borderColor: 'slateblue',
			fill: false
		}]
	};
	
	const options = {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	};
	
	return (
		<Line data={ data } options={ options } />
	);
}